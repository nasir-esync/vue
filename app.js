console.log('sss');

const app = Vue.createApp({
    // data, functions
    // template: "<h2>I am a Template</h2>"
    data() {
        return {
            url: 'http://www.thenetninja.co.uk',
            showBooks: true,
            books: [
                {
                    title: 'The final Empiror',
                    author: "Brandon Sandrom",
                    img: 'assets/1.jpg',
                    isFav: true
                },
                {
                    title: 'The Harry Porter',
                    author: "Harry",
                    img: 'assets/1.jpg',
                    isFav: true
                },
                {
                    title: 'The Harry Porter II',
                    author: "Harry2",
                    img: 'assets/1.jpg',
                    isFav: true
                }
            ],
            title: 'The final Empiror',
            author: "Brandon Sandrom",
            age: 20,
            x: 0,
            y: 0
        }
    },
    methods: {
        changeTitle() {
            // alert('you click');
            this.title = 'New Word change by method';
        },
        handleEvent(e) {
            console.log(e, e.type);
        },
        handleMousemove(e) {
            this.x = e.offsetX;
            this.y = e.offsetY;
        },
        toggleFav(book) {
            book.isFav = !book.isFav;
        }
    },
    computed: {
        filteredBooks() {
            return this.books.filter((book) => book.isFav);
        }
    }

});

app.mount("#app");